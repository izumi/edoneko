///
/// Retrieve Edo neko blog RSS feed and merge its old RSSs.
///

extern crate time;
extern crate xml;
extern crate libc;
#[macro_use]
extern crate slog;
extern crate slog_stream;
extern crate slog_stdlog;
#[macro_use]
extern crate log;
extern crate hyper;


use std::env;
use std::path::{Path, PathBuf};
use std::fs::{self, File, OpenOptions};
use std::io;
use std::process;

mod fetch;
mod neko;
mod timestamp;
mod logfile;

use fetch::HTTPResult;
use timestamp::{FileTimestamp};

const FEED_URI: &'static str = "http://cats-blog.com/?feed=rss2";

const FEED_DIR: &'static str = "/var/cache/izumi";
const OUT_FILE: &'static str = "edoneko.xml";
const NEW_OUT_FILE: &'static str = "edoneko-new.xml";


fn main() {

    let outdir = Path::new(FEED_DIR);
    let cachedir = outdir.join("pool");

    logfile::setup_logfile(&cachedir.join("neko.log"));

    let ipath = cachedir.join("last.xml");

    match fetch::fetch(FEED_URI, &ipath) {
	Ok(HTTPResult::Fetched(true)) => {}
	Ok(HTTPResult::Fetched(false)) => { process::exit(0); }
	Err(e) => {
	    error!("Failed to retrieve <{}> as {:?}: {}", FEED_URI, ipath, &e);
	    process::exit(1);
	}
    }

    let infile = env::args_os().nth(1);

    let rss = infile.as_ref().map_or(&ipath as &Path, |f| Path::new(f));

    // Back up RSS files
    if let Err(e) = shift_files(&rss, &cachedir, 9) {
	warn!("Failed to shift old RSS files: {}", &e);
    }

    let newpath = outdir.join(NEW_OUT_FILE);
    let files = [outdir.join(OUT_FILE)];
    info!("Merge {:?} with {:?} => {:?}", &rss, &files, newpath);
    // Convert to reference
    let paths: Vec<&Path> = files.iter().rev().map(|i| i.as_path()).collect();

    match merge_rss(&rss, &newpath, &paths, 40) {
	Ok(count) => {
	    debug!("{} items processed", count);
	}
	Err(e) => {
	    error!("Failed to merge RSS: {:?}", &e);
	    process::exit(1);
	}
    }

    // Update target feed atomically
    if let Err(e) = fs::rename(&newpath, &files[0]) {
	warn!("Rename {:?} to {:?} failed: {}", &newpath, &files[0], &e);
	process::exit(1);
    }
}

//
// Shift files ... 2.xml <= 1.xml <= 0.xml <= new
// new is copied to 0.xml with modified time preserved
// while n.xml is renamed to (n+1).xml
//
fn shift_files(new: &Path, cachedir: &Path, n: usize) -> io::Result<Vec<PathBuf>> {
    let mut files = Vec::with_capacity(n);
    let mut old = cachedir.join(format!("{}.xml", n));
    for i in (0 .. n).rev() {
	let new = cachedir.join(format!("{}.xml", i));
	if let Err(e) = fs::rename(&new, &old) {
	    if e.kind() == io::ErrorKind::NotFound {
		trace!("Skip non-existent file {:?}", &new);
	    } else {
		warn!("Rename {:?} to {:?} failed: {}", &new, &old, &e);
	    }
	} else {
	    files.push(old.to_path_buf());
	}
	old = new;
    }

    match copy_file(&new, &old) {
	Ok(_) => { files.push(old.to_path_buf()); }
	Err(e) => {
	    error!("Copy {:?} to {:?} failed: {:?}", &new, &old, &e);
	    return Err(e);
	}
    }

    Ok(files)
}

fn copy_file(src: &Path, dest: &Path) -> io::Result<()> {

    fs::copy(&src, &dest)?;

    let meta = fs::metadata(src)?;

    let mtime = FileTimestamp::new_from_system_time(meta.modified()?)?;
    timestamp::set_timestamp(dest, FileTimestamp::Preserve, mtime)
}

fn merge_rss(base: &Path, rss: &Path, files: &Vec<&Path>, maxitems: usize)
	-> neko::Result<usize> {

    match File::open(base) {
	Ok(f) => {
	    match OpenOptions::new().write(true).truncate(true).create(true)
				.open(rss) {
		Ok(mut out) => {
		    neko::RssMerge::new(&mut out, maxitems).process(f, &files)
		}
		Err(e) => {
		    error!("Failed to write to output RSS {:?}: {:?}", rss, &e);
		    process::exit(1);
		}
	    }
	},
	Err(e) => {
	    error!("Unable to read original RSS {:?}: {:?}", base, &e);
	    process::exit(1);
	}
    }
}
