
use std::io::{self, Read};
use std::fs::{self, OpenOptions};
use std::path::Path;
//
// Both time crate and std::time module are necessary.
use std::time::{Duration as StdDuration, SystemTime as StdSystemTime, UNIX_EPOCH as STD_UNIX_EPOCH};

use time;
use libc;

//use hyper::Url;
use hyper::client::Client;
use hyper::header::{IfModifiedSince, LastModified, HttpDate};
//use hyper::client::request::Request;
use hyper::client::response::Response;
use hyper::status::StatusCode;
use hyper::method::Method;

use timestamp;

pub enum HTTPResult {
    Fetched(bool),
}

pub fn fetch(uri: &str, path: &Path) -> io::Result<HTTPResult> {

    let mut resp = get_response(uri, path)?;
    match resp.status {
	StatusCode::Ok => {
	    let total = save_response(&mut resp, path)?;
	    info!("Response {:?}: Stored {} bytes", &resp.status, total);
	    Ok(HTTPResult::Fetched(true))
	}
	StatusCode::NotModified => {
	    info!("Resource not modified");
	    Ok(HTTPResult::Fetched(false))
	}
	_ => {
	    error!("Unexpected response status {:?}", &resp.status);
	    Err(io::Error::new(io::ErrorKind::Other,
			       "Unexpected response code"))
	}
    }
}

fn get_response(uri: &str, path: &Path) -> io::Result<Response> {
    let mut client = Client::new();
    client.set_read_timeout(Some(StdDuration::from_secs(40)));
    client.set_write_timeout(Some(StdDuration::from_secs(40)));

    let mut builder = client.request(Method::Get, uri);
    if let Some(h) = modified_http_date(path) {
	builder = builder.header(IfModifiedSince(h));
    }

    builder.send().map(|r| r)
	.map_err(|e| io::Error::new(io::ErrorKind::Other, e))
}

fn modified_http_date(path: &Path) -> Option<HttpDate> {

    match fs::metadata(path) {
	Ok(meta) => {
	    match meta.modified() {
		Ok(mtime) => {
		    match to_utc_tm(mtime) {
			Ok(ts) => {
			    debug!("If-Modified-Since: {:?}",
				  time::strftime("%Y-%m-%d %H:%M:%S %Z", &ts));
			    Some(HttpDate(ts))
			}
			Err(e) => {
			    warn!("Failed to get timestamp {:?}: {}", path, &e);
			    None
			}
		    }
		}
		Err(e) => {
		    warn!("Failed to get timestamp {:?}: {:?}", path, &e);
		    None
		}
	    }
	},
	Err(e) => {
	    if e.kind() != io::ErrorKind::NotFound {
		info!("Metainfo of {:?} is not available: {}", path, &e);
	    }
	    None
	}
    }
}

/// Convert std::time::SystemTime into time::Tm
fn to_utc_tm(t: StdSystemTime) -> io::Result<time::Tm> {
    match t.duration_since(STD_UNIX_EPOCH) {
	Ok(d) => {
	    let ts = time::Timespec {
		sec: d.as_secs() as i64,
		nsec: d.subsec_nanos() as i32,
	    };
	    Ok(time::at_utc(ts))
	}
	Err(e) => {
	    Err(io::Error::new(io::ErrorKind::Other, e))
	}
    }
}

fn save_response(resp: &mut Response, path: &Path) -> io::Result<usize> {
    let r = {
	let r = OpenOptions::new()
		    .write(true).create(true)
		    .truncate(true).open(path);
	match r {
	    Ok(mut fh) => store_response(resp, &mut fh),
	    Err(e) => {
		error!("Failed to create {:?}: {:?}", path, &e);
		Err(e)
	    }
	}
    };

    // Change file timestamp after close().
    if r.is_ok() {
	if let Some(&LastModified(HttpDate(ref mtime))) = resp.headers.get::<LastModified>() {
	    debug!("Set Last-Modified: {:?}",
		  time::strftime("%Y-%m-%d %H:%M:%S %Z", mtime));
	    set_modified_time(path, mtime)?;
	}
    }

    r
}

fn store_response(resp: &mut Response, out: &mut io::Write) -> io::Result<usize> {
    let mut total = 0;
    let mut buf = [0u8; 8 * 1024];
    loop {
	let size = resp.read(&mut buf[..])?;
	if size <= 0 {
	    break;
	}
	out.write(&buf[..size])?;
	total += size;
    }

    Ok(total)
}

fn set_modified_time(path: &Path, mtime: &time::Tm) -> io::Result<()> {
    let t = mtime.to_timespec();

    let ts = libc::timespec {
	tv_sec: t.sec,
	tv_nsec: t.nsec as i64,
    };

    timestamp::set_timestamp(path,
			     timestamp::FileTimestamp::Preserve,
			     timestamp::FileTimestamp::At(ts))
}
