
use std::str;
use std::io;
use std::result;
use std::fmt;
use std::convert::From;
use std::collections::HashSet;
use std::fs::File;
use std::path::Path;

use xml::reader::{self, EventReader, XmlEvent};
use xml::attribute::OwnedAttribute;
use xml::escape::{escape_str_attribute, escape_str_pcdata};
use xml::name::OwnedName;
use xml::namespace::Namespace;

#[derive(Debug)]
pub enum RssError {
    Io(io::Error),
    Read(reader::Error),
}

impl From<io::Error> for RssError {
    fn from(e: io::Error) -> Self {
	RssError::Io(e)
    }
}

impl From<reader::Error> for RssError {
    fn from(e: reader::Error) -> Self {
	RssError::Read(e)
    }
}

pub type Result<T> = result::Result<T, RssError>;

#[derive(Copy, Clone, Debug)]
struct NullWriter {}

/// Discard all output
impl io::Write for NullWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
	Ok(buf.len())
    }
    fn flush(&mut self) -> io::Result<()> {
	Ok(())
    }
}

pub struct RssMerge<'a, T: 'a + io::Write> {
    max_items: usize,
    item_count: usize,
    mergee: bool,
    // Buffer for item element.
    // Duplicated item element will be dropped.
    // If this is Some, we are in an item element.
    item: Option<Vec<u8>>, // Vec<u8> impls io::Write. String does not.
    skip: bool,
    // link element in item element.
    link: Option<String>,
    // # of processed tags
    count: u32,
    opened: bool,
    out: &'a mut T,
    null: NullWriter,
}

impl<'a, T: 'a + io::Write> RssMerge<'a, T> {
    pub fn new(out: &mut T, maxitems: usize) -> RssMerge<T> {
	RssMerge {
	    max_items: maxitems,
	    item_count: 0,
	    mergee: false,
	    item: None,
	    link: None,
	    skip: false,
	    count: 0,
	    opened: false,
	    out: out,
	    null: NullWriter {},
	}
    }

    pub fn process<R: io::Read>(&mut self, src: R, files: &Vec<&Path>) -> Result<usize> {
	let mut processed = HashSet::new();

	self.process_generic(src, &mut processed, files)
    }

    pub fn process_generic<R: io::Read>(&mut self, src: R,
				processed: &mut HashSet<String>,
				files: &Vec<&Path>) -> Result<usize> {

	for ev in EventReader::new(src) {
	    self.process_event(ev?, processed, files)?;
	}

	self.out.flush()?;

	Ok(self.item_count)
    }

    fn process_event(&mut self, ev: XmlEvent, processed: &mut HashSet<String>,
		     files: &Vec<&Path>) -> Result<()> {
	match ev {
	    XmlEvent::StartDocument { version, encoding, .. } => {
		self.write_fmt(
		    format_args!("<?xml version=\"{}\" encoding=\"{}\"?>\n",
			version, escape_str_attribute(&encoding)))
	    }
	    XmlEvent::ProcessingInstruction { name, data } => {
		if let Some(text) = data {
		    self.write_fmt(format_args!("<?{} {}?>", &name, &text))
		} else {
		    self.write_fmt(format_args!("<?{}?>", &name))
		}
	    }
	    XmlEvent::StartElement { name, attributes, namespace: ns } => {
		match &name.local_name[..] {
		    "item" if name.prefix.is_none() => self.start_item(),
		    "link" if name.prefix.is_none() => self.start_link(),
		    &_ => (),
		}
		self.print_tag(&name, &attributes)?;
		if self.count == 0 {
		    // Root element
		    self.print_namespaces(&ns).map_err(|e| RssError::Io(e))?;
		}
		self.opened = true;
		self.count += 1;
		Ok(())
	    }
	    XmlEvent::EndElement { ref name } => {
		if &name.local_name[..] == "channel" {
		    debug!("MERGE {:?}", &files);
		    self.process_mergees(processed, files)?;
		}
		let r = self.end_tag(name)?;
		match &name.local_name[..] {
		    "item" if name.prefix.is_none() => self.end_item(),
		    "link" if name.prefix.is_none() => {
			self.end_link(processed); Ok(())
		    },
		    &_ => Ok(r),
		}.map_err(|e| e.into())
	    }
	    XmlEvent::Characters(text) => {
		if text != "" {
		    self.close_tag()?;
		}
		if let Some(link) = self.link.as_mut() {
		    link.push_str(&text);
		}
		self.write(escape_str_pcdata(&text).as_bytes())
	    }
	    XmlEvent::Whitespace(data) => {
		self.close_tag()?;
		self.write(data.as_bytes())
	    }
	    XmlEvent::CData(text) => {
		self.close_tag()?;
		self.write_fmt(format_args!("<![CDATA[{}]]>", &text))
	    }
	    XmlEvent::Comment(text) => {
		self.close_tag()?;
		// Need escape?
		self.write_fmt(format_args!("<!--{}-->", &text))
	    }
	    XmlEvent::EndDocument => {
		self.close_tag()?;
		self.write(b"\n")
	    }
	}
    }

    fn write_fmt(&mut self, format: fmt::Arguments) -> Result<()> {
	self.current_out().write_fmt(format).map_err(|e| e.into())
    }

    fn write(&mut self, buf: &[u8]) -> Result<()> {
	self.current_out().write(buf).map(|_| ()).map_err(|e| e.into())
    }

    fn close_tag(&mut self) -> Result<()> {
	if self.opened {
	    self.opened = false;
	    self.write(b">")
	} else {
	    Ok(())
	}
    }

    fn print_tag(&mut self, name: &OwnedName, attrs: &Vec<OwnedAttribute>)
	    -> io::Result<()> {
	let mut out = self.current_out();
	if let Some(ref prefix) = name.prefix {
	    write!(out, "<{}:{}", prefix, &name.local_name)?;
	} else {
	    write!(out, "<{}", &name.local_name)?;
	}

	for attr in attrs {
	    write!(out, " {}=\"{}\"",
		   &attr.name, escape_str_attribute(&attr.value))?;
	}

	Ok(())
    }

    fn current_out(&mut self) -> &mut io::Write {
	if self.mergee && self.item.is_none() {
	    &mut self.null
	} else {
	    self.item.as_mut()
		.map(|o| o as &mut io::Write)
		.unwrap_or(&mut self.out as &mut io::Write)
	}
    }

    fn print_namespaces(&mut self, ns: &Namespace) -> io::Result<()> {
	let mut out = self.current_out();
	for (prefix, uri) in ns {
	    match prefix {
		"" | "xml" | "xmlns" => (),
		_ => {
		    write!(out, "\n\txmlns:{}=\"{}\"",
			   prefix, escape_str_attribute(uri))?;
		}
	    }
	}

	Ok(())
    }

    fn end_tag(&mut self, name: &OwnedName) -> Result<()> {
	if self.opened {
	    self.opened = false;
	    self.write(b" />")
	} else {
	    if let Some(ref prefix) = name.prefix {
		self.write_fmt(format_args!("</{}:{}>",
					    prefix, &name.local_name))
	    } else {
		self.write_fmt(format_args!("</{}>", &name.local_name))
	    }
	}
    }

    fn start_item(&mut self) {
	self.item = Some(Vec::with_capacity(2048));
	self.skip = self.item_count >= self.max_items;
    }

    fn end_item(&mut self) -> io::Result<()> {
	if let Some(item) = self.item.take() {
	    if !self.skip {
		self.item_count += 1;
		self.out.write(&item)?;
	    }
	}
	Ok(())
    }

    fn start_link(&mut self) {
	if self.item.is_some() {
	    self.link = Some(String::with_capacity(256));
	}
    }

    fn end_link(&mut self, processed: &mut HashSet<String>) {
	if self.item.is_some() && !self.skip {
	    if let Some(link) = self.link.take() {
		// Skip current item if this is a duplicated link.
		self.skip = !processed.insert(link);
	    }
	}
    }

    fn process_mergees(&mut self, processed: &mut HashSet<String>,
		       files: &Vec<&Path>) -> Result<()> {
	let empty = Vec::with_capacity(0);
	for path in files {
	    self.out.flush()?;
	    if self.item_count >= self.max_items {
		break;
	    }
	    write!(self.out, "<!-- MERGE {:?} -->\n", path)?;
	    let mut m = RssMerge::new(self.out,
				      self.max_items - self.item_count);
	    m.mergee = true;
	    match File::open(path) {
		Ok(f) => {
		    m.process_generic(f, processed, &empty)?;
		}
		Err(e) => {
		    return Err(io::Error::new(e.kind(),
			format!("Failed to ead {:?}: {}", path, e)).into());
		}
	    }
	    self.item_count += m.item_count;
	}

	Ok(())
    }
}
