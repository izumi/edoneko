///
/// Manipulate file timestamp.
///

use std::time;
use std::path::Path;
use std::io;
use std::ffi::{CString};

use libc;

// libc 0.2.20 does not provie UTIME_* constants.
const UTIME_NOW: libc::time_t = ((1i64 << 30) - 1i64);
const UTIME_OMIT: libc::c_long = ((1i64 << 30) - 2i64);

#[derive(Clone, Copy)]
pub enum FileTimestamp {
    /// Specified timestamp
    At(libc::timespec),
    #[allow(dead_code)]
    /// Use current time
    CurrentTime,	// UTIME_NOW
    /// Preserve file timestamp
    Preserve,		// UTIME_OMIT
}

impl FileTimestamp {
    pub fn new_from_system_time(t: time::SystemTime) -> io::Result<Self> {
	let dur = t.duration_since(time::UNIX_EPOCH)
	    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;

	let ts = libc::timespec {
	    tv_sec: dur.as_secs() as libc::time_t,
	    tv_nsec: dur.subsec_nanos() as libc::c_long,
	};
	Ok(FileTimestamp::At(ts))
    }

    fn time_spec(&self) -> libc::timespec {
	match self {
	    &FileTimestamp::At(ts) => ts,
	    &FileTimestamp::CurrentTime => {
		libc::timespec {
		    tv_sec: UTIME_NOW,
		    tv_nsec: 0,
		}
	    }
	    &FileTimestamp::Preserve => {
		libc::timespec {
		    tv_sec: 0,
		    tv_nsec: UTIME_OMIT as libc::c_long,
		}
	    }
	}
    }
}

pub fn set_timestamp(file: &Path, atime: FileTimestamp, mtime: FileTimestamp)
    	-> io::Result<()> {
    let cpath = path_to_cstring(file)?;

    // Atime and mtime
    let times = [atime.time_spec(), mtime.time_spec()];
    let r = unsafe {
	libc::utimensat(libc::AT_FDCWD, cpath.as_ptr(),
		times.as_ptr() as *const _, 0)
    };

    if r == 0 {
	Ok(())
    } else {
	Err(io::Error::last_os_error())
    }
}

fn path_to_cstring(path: &Path) -> io::Result<CString> {
    if let Some(cpath) = path.to_str() {
	CString::new(cpath)
	    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))
    } else {
	Err(io::Error::new(io::ErrorKind::Other,
			   "Path unicode conversion error"))
    }
}

