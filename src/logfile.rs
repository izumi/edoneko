
use std::io;
use std::path::{Path};
use std::fs::{OpenOptions};

use time;
use slog::{self, DrainExt};
use slog_stream;
use slog_stdlog;

// Custom log format
struct LogFormat;

impl slog_stream::Format for LogFormat {
    fn format(&self, io: &mut io::Write, rinfo: &slog::Record,
	      _pairs: &slog::OwnedKeyValueList) -> io::Result<()> {
	let ts = time::strftime("%Y-%m-%d %H:%M:%S", &time::now());
	let tsstr = ts.as_ref().map(|s| &s[..]).unwrap_or("----");
	let msg = format!("{} {}[{}] - {}\n",
			  tsstr, rinfo.level(), rinfo.module(), rinfo.msg());
	io.write_all(msg.as_bytes()).map(|_| ())
    }
}

pub fn setup_logfile(path: &Path) {
    let fh = OpenOptions::new().create(true).append(true).open(path)
	    .expect(&format!("Failed to open log file: {:?}", &path));
    let drain = slog_stream::stream(fh, LogFormat).fuse();
    // Decorate drain
    let f = slog::Filter::new(drain, filter_log);
    let logger = slog::Logger::root(f, o!());
    slog_stdlog::set_logger(logger).unwrap();
}

#[cfg(not(debug_assertions))]
fn filter_log(rec: &slog::Record) -> bool {
    rec.level() <= slog::Level::Info
}

#[cfg(debug_assertions)]
fn filter_log(rec: &slog::Record) -> bool {
    rec.level() <= slog::Level::Info
	|| rec.level() <= slog::Level::Debug
	&& !rec.module().starts_with("hyper::")
}
