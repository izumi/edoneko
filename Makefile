
    PATH += ${HOME}/.cargo/bin
    #CARGO_HOME = /archives/src/rust/cargo
    CARGO = cargo
    PROG = edoneko
    CFLAGS = -Wall

all: tags
	$(CARGO) build

build run test doc:
	$(CARGO) $@

tags: src/*.rs
	ctags src/*.rs

clean:
	$(CARGO) $@
	rm -f tags cscope.out
